package Core;

/**
 * Holds one item. Will give out the item, if given the right item
 * (just a name is necessary to evaluate this).
 * 
 * When he/she has the item, he/she will be "satisfied".
 * 
 * @author Roger Veldman
 */
public class NPC
{
    boolean hasItem;
    String name, itemWantName, advice;
    public static int peopleSatisfied = 0;
    public static int peopleTotal = 0;

    /**
     * Create an NPC
     * @param Name for the NPC.
     * @param Item he/she wants.
     * @param The phrase the character will say after satisfied.
     */
    public NPC(String name, String itemWantName, String advice){
        hasItem = false;
        this.name = name;
        this.itemWantName = itemWantName;
        this.advice = advice;
        peopleTotal++;
    }

    /**
     * Accessor method for the String name.
     */
    public String getName(){
        return name;
    }

    /**
     * Marks the hasItem if the right item is given.
     * @return Return true if given item is what the person wants and if not
     * return false.
     */
    public boolean tryGive(String itemGiveName){
        if (itemGiveName.equals(itemWantName)){
            satisfy();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Marks hasItem as true.
     */
    public void satisfy(){

        peopleSatisfied++;
        hasItem = true;
    }

    /**
     * Gives a primitive dialogue system for this character.
     */
    public String talk(){
        if (hasItem){
            return advice;
        } else {
            return "Hi, I'm " + name + ". Could you find my " + itemWantName +
            ".\nI seem to have misplaced it.";
        }
    }
}
