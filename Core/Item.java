package Core;
/**
 * Represents an item in the world.
 * Can be edible or not.
 * 
 * Allotted more time, I would have used a class called Food (that extends Item) to
 * provide functionality for this, but alas, I have not used these enough to know them in detail.
 * 
 * @author Roger Veldman
 */
public class Item
{
    private String description;
    int weight;
    boolean edible;
    
    /**
     * Create an item with a description and a weight.
     */
    public Item(String description, int weight){
        this.description = description;
        this.weight = weight;
        edible = false;
    }
    
    /**
     * Create an item with a description and a weight, as well as the "edible" specification.
     */
    public Item(String description, int weight, boolean edible){
        this.description = description;
        this.weight = weight;
        this.edible = edible;
    }
    
    /**
     * Return a description of the object.
     */
    public String getDescription(){
        return description;
    }
    
    /**
     * Return the weight of the object.
     */
    public int getWeight(){
        return weight;
    }
    
    /**
     * Return the boolean "edible".
     */
    public boolean isEdible(){
        return edible;
    }
    
    /**
     * Mutator method for boolean edible.
     */
    public void setEdible(boolean edible){
        this.edible = edible;
    }
}
