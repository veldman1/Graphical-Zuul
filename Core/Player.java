package Core;
import java.lang.StringBuilder;
import java.util.ArrayList;
import java.util.HashMap;
/**
 * Stores all of the player's information.
 * @author Roger Veldman
 */
public class Player
{
    private Room currentRoom, lastRoom;
    private HashMap<String,Item> playerItems;
    private int energy, maxEnergy, strength;

    /**
     * Create a player with an initial room.
     */
    public Player(Room startRoom){
        currentRoom = startRoom;
        energy = 200;
        maxEnergy = 200;
        strength = 1;
        playerItems = new HashMap<String, Item>();
    }

    /**
     * @return Return true if success, and false if the item is not edible or not found.
     */
    public boolean eat(String name){
        if (hasItem(name)){
            if (playerItems.get(name).isEdible()){
                addEnergy(50);
                return true;
            }

        }
        return false;
    }
    
    /**
     * Set a room for the player.
     */
    public void setRoom(Room newRoom){
        lastRoom = currentRoom;
        currentRoom = newRoom;
    }

    /**
     * Go back a room.
     * Idiot-proofed to ensure the last room isn't null.
     * @return Return if the last room was null or not.
     */
    public boolean backRoom(){
        if (lastRoom!=null){
            Room lastLastRoom = currentRoom;
            currentRoom = lastRoom;
            lastRoom = lastLastRoom;
            return true;
        }
        return false;
    }

    /**
     * Return the player's current room.
     */
    public Room getRoom(){
        return currentRoom;
    }

    /**
     * Accessor method for int energy.
     */
    public int getEnergy(){
        return energy;
    }

    /**
     * Adds energy to int energy.
     * To subtract, enter a negative value.
     */
    public void addEnergy(int add){
        energy += add;
        
        // caps energy
        if (energy > getMaxEnergy()){
            energy = getMaxEnergy();
        }
    }

    /**
     * Return the maximum energy this player can have.
     */
    public int getMaxEnergy(){
        return maxEnergy;
    }

    /**
     * LEVEL UP
     * Adds strength so you can lift more things.
     */
    public void nextStrengthLevel(){
        strength++;
    }

    /**
     * Return the strength level.
     */
    public int getStrengthLevel(){
        return strength;
    }

    /**
     * Return the calculated value for how much you can hold in your bag.
     */
    public int getMaxWeight(){
        return 15 + 5 * strength;
    }

    /**
     * @return Return true if successful and false if not.
     */
    public boolean addItem(String name,Item item){
        if ((item.getWeight() + getWeight())<=getMaxWeight()){
            playerItems.put(name,item);
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Remove an item from the player's bag.
     */
    public void removeItem(String name){
        if (playerItems.get(name)!=null){
            playerItems.remove(name);
        }
    }

    /**
     * Return true/false for if the player has the key in the playerItems hashmap.
     */
    public boolean hasItem(String name){
        return playerItems.containsKey(name);
    }

    /**
     * Return a list of all the items (keys) in the playerItems hashmap.
     */
    public ArrayList<String> getItemList(){
        ArrayList<String> itemList =  new ArrayList<String>();
        for (String name:playerItems.keySet()){
            itemList.add(name);
        }
        return itemList;
    }

    /**
     * Return the sum of all the item weights of the in the player's bag.
     */
    public int getWeight(){
        int weight = 0;
        for (Item item:playerItems.values()){
            weight += item.getWeight();
        }
        return weight;
    }

    /**
     * Return a string for printing of all the player's items.
     */
    public String getItemsAsString(){
        StringBuilder makeString = new StringBuilder("Contents:");
        for (String name:playerItems.keySet()){
            makeString.append("\n" + name);
        }
        return makeString.toString();
    }

    /**
     * Get a name for the player.
     * Return null if there isn't one... (I would fix this if I had time. I hate nulls).
     */
    public Item getItem(String name){
        if (hasItem(name)){
            return playerItems.get(name);
        } else
            return null;
    }
}