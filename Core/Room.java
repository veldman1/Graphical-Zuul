package Core;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
/**
 * Represents an area in the game. 
 * @author Roger Veldman
 */

public class Room 
{
    private String description;
    private HashMap<String, Room> exits;// stores exits of this room.
    private HashMap<String, Room> lockedExits;
    private HashMap<String, Item> items;
    private HashMap<String,NPC> people;
    private String name;
    private boolean discovered;

    /**
     * Create a room described "description". Initially, it has
     * no exits. "description" is something like "a kitchen" or
     * "an open court yard".
     * @param description The room's description.
     */
    public Room(String name, String description)
    {
        this.description = description;
        exits = new HashMap<String, Room>();
        lockedExits = new HashMap<String, Room>();
        items = new HashMap<String, Item>();
        people = new HashMap<String,NPC>();
        this.name = name;
        discovered = false;
    }
    
    public String getName() {
    	return name;
    }

    /**
     * Define an exit from this room.
     * @param direction The direction of the exit.
     * @param neighbor  The room to which the exit leads.
     */
    public void setExit(String direction, Room neighbor) 
    {
        exits.put(direction, neighbor);
    }

    /**
     * Define a locked exit. Ex. 6.44
     */
    public void setLockedExit(String direction, Room neighbor) 
    {
        lockedExits.put(direction, neighbor);
    }

    /**
     * Unlock an exit with a specified name.
     * This method, unfortanately has no way to tell if successful
     * or not.
     * 
     * If I have more time, I will create a way to not swallow
     * this problem, but deal with it.
     */
    public void unlockExit(String name){
        if (lockedExits.containsKey(name)){
            exits.put(name,lockedExits.get(name));
            lockedExits.remove(name);
        }
    }

    /**
     * @return The short description of the room
     * (the one that was defined in the constructor).
     */
    public String getShortDescription()
    {
        return description;
    }

    /**
     * Return a description of the room in the form:
     *     You are in the kitchen.
     *     Exits: north west
     * @return A long description of this room
     */
    public String getLongDescription()
    {
        return description + "\n" + getExitString();
    }

    /**
     * Return a string describing the room's exits, for example
     * "Exits: north west".
     * 
     * Modifications used to use StringBuilder class.
     * 
     * @return Details of the room's exits.
     */
    private String getExitString()
    {
        StringBuilder returnString = new StringBuilder("");
        if (exits.size()!=0){
            returnString.append("Exits:");
            Set<String> keys = exits.keySet();
            for(String exit : keys) {
                returnString.append(" " + exit);
            }
        }

        if (lockedExits.size()!=0){
            returnString.append("\nLocked Exits:");
            Set<String> lockKeys = lockedExits.keySet();
            for(String lockedExit : lockKeys) {
                returnString.append(" " + lockedExit);
            }
        }
        return returnString.toString();
    }

    /**
     * Return the room that is reached if we go from this room in direction
     * "direction". If there is no room in that direction, return null.
     * @param direction The exit's direction.
     * @return The room in the given direction.
     */
    public Room getExit(String direction) 
    {
        return exits.get(direction);
    }

    /**
     * Return an item as specified by a name (that should be stored in the hashmap).
     */
    public Item getItemByName(String name){
        return items.get(name);
    }

    /**
     * Add an item to the hashmap.
     * @param A name for the item.
     * @param A description for the item.
     * @param A weight for the item.
     */
    public void addItem(String name,String description,int weight){
        items.put(name,new Item(description,weight));
    }

    /**
     * Add an item to the hashmap and mark it as food.
     * @param A name for the item.
     * @param A description for the item.
     * @param A weight for the item.
     */
    public void addFoodItem(String name,String description,int weight){
        items.put(name,new Item(description,weight,true));
    }

    /**
     * Return a list of the item names (keys) in the items hashmap.
     */
    public ArrayList<String> getItemNames(){
        ArrayList<String> names = new ArrayList<String>();
        for (String name:items.keySet()){
            names.add(name);
        }
        return names;
    }

    /**
     * Remove an item by name in the items hashmap.
     * 
     * Again, this method swallows the problem of having a key
     * not in the items hashmap.
     * 
     * If I have more time, I will make this not do this.
     */
    public void remove(String name){
        if (items.get(name)!=null){
            items.remove(name);
        }
    }

    /**
     * Put an NPC in the people map.
     */
    public void addPerson(NPC person){
        people.put(person.getName(),person);
    }

    /**
     * Attempt to give an NPC in this room an item.
     * @return Return true if the item is correct,
     * or false if not or if the person is not in the room
     */
    public boolean attemptGive(String personName, String itemName){
        if (people.containsKey(personName)){
            return people.get(personName).tryGive(itemName);
        } else {
            return false;
        }
    }

    /**
     * Get the NPC's verbal response.
     */
    public String getTalk(String name){
        if (people.containsKey(name)){
            return people.get(name).talk();
        } else {
            return "Don't know who you're talking about.";
        }
    }

    /**
     * Return a Set of the npc names in this room.
     */
    public Set<String> getNpcNames(){
        return people.keySet();
    }
    
    public Map<String, Room> getExits(){
    	return exits;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        final Room other = (Room) obj;
        if (!other.name.equals(this.name)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

	public boolean isDiscovered() {
		return discovered;
	}

	public void setDiscovered(boolean discovered) {
		this.discovered = discovered;
	}
}